#Order Management Application#


## **Description** ##


Order Management Application  is a project designed to calculate the  total amount to be paid for the list of orders placed. 
The project uses maven as build system to build, manage the dependencies and execute the tests.
Also the project uses Junit for test case design and execution.
 
## Environment ##

The project uses the following tools and APIs

* java - 1.7 or higher
* JUnit - 4.11


## **Directory Structure** 

**src/main :**       contains different modules of application. 

                     Application is implemented in a module driven approach. Please find the design architecture strategy and advantages in Architecture Description Document
      	      	    
    	- src/main/java : contains the implementation of app and model modules
      		
             
src/test :   contains all the test cases. Test Suites  are also present in this package.
    	
    	 - src/test/java :   contains all the test scripts along with JUnit Test suites. A MasterTestSuite is present which executes all test suites containing unit test cases.



## **Steps to run the Application  ** 

  1. Clone and execute Maven Clean and Build**
     
      Using IDE : 
      Clone the repository and Import the Project into any IDE (Eclipse etc..) and execute maven clean and build. Please ensure that all the maven dependencies are installed and added to classpath. In case  of Eclipse IDE, If we are using m2e plugin, it will automatically update the classpath. 
 
      Using Maven Command Line :
      This step can also be done with maven commandline using maven lifecycle commands
 
  2. Execute the Main Class of Application
  
      The main class of the application is present under org.teksystems.ordermanagement.app package in the project. The class file name is **OrderManagementApplication.java**
      After successful execution, order summary and total amount will be displayed on the standard output stream.
      
      Also, the previous version file Foo.java given in the coding assignment is present in the same package just for refernce.
 
 ## **Steps to run test cases **
 
   Test Cases can be executed either using Maven or Junit Test Runner Plugin
 
      1) Using Maven
 		
 		  From IDE (Eclipse etc)-  If the maven build is successful and all the dependencies are resolved in the above steps, just Run Maven test from Run Configurations. 
 
                From Command line -   using the Maven Build system, we can execute using command mvn test.
            
      2) Using Junit Plugin From IDE (Eclipse,etc..)       
     
          The TestSuites can be launched and executed through Junit Run Configuration from any IDE (Eclipse,etc..). MasterTestSuite.java is the master suite that executes all test suites containing several unit test cases.
          Also, each package contains its own test suite. RightClick on any of the either MasterTestSuite.java or individual test suite file and RunAs Junit Tests. Also, the test classes mentioned in the suites can be executed as a seperate Junit Tests.
 
 
### Documents or References ###


Please find the below documents in doc folder under project directory.

* OrderManagementApplicationArchitectureGuide -  Document that describes the architecture , its goals, principle,  modules and class diagrams