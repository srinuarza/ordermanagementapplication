/**
 * 
 */
package org.teksystems.ordermanagement.app.utils.tax;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Test case for {@link org.teksystems.ordermanagement.taxcalculations.LocalTaxCalculator}
 * 
 * @author srinivas
 */
public class TaxCalculatorTest {

	/** Creates a test fixture */
	TaxCalculator taxcalc = new TaxCalculator();
	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.taxcalculations.LocalTaxCalculator#calculateTax(double, double, boolean)}.
	 */
	@Test
	public void testCalculateTax()
	{
		assertEquals(7.15, taxcalc.calculateTax(47.50, 0.10, true), 0);
		assertEquals(0.50, taxcalc.calculateTax(10.00, 0, true), 0);
		assertEquals(1.50, taxcalc.calculateTax(14.99, 0.10, false),0);
		assertEquals(0, taxcalc.calculateTax(12.49, 0, false), 0);
	}

}
