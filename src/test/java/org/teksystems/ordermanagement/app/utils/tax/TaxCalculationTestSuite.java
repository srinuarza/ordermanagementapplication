package org.teksystems.ordermanagement.app.utils.tax;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TaxCalculatorTest.class, MathHelperTest.class})
public class TaxCalculationTestSuite {

}
