/**
 * 
 */
package org.teksystems.ordermanagement.app.ordermanagement.billing;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.teksystems.ordermanagement.model.items.BookItem;
import org.teksystems.ordermanagement.model.items.FoodItem;
import org.teksystems.ordermanagement.model.items.Item;

/**
 * Test case for {@link org.teksystems.ordermanagement.billingdomain.Receipt}
 *
 * @author
 */
public class ReceiptTest {

	/** Creates a test fixture */
	List<Item> prodList;
	BookItem book;
	FoodItem food;
	Receipt receipt;
	
	/** Initializes the test fixture */
	@Before
	public void setUp()
	{
		book = new BookItem("book", 40.00, true, 1);
		food = new FoodItem("box of chocolates", 30.00, false, 1);
		prodList = new ArrayList<Item>();
		prodList.add(book);
		prodList.add(food);
		receipt = new Receipt(prodList, 9.00, 45.00);
	}
	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.billingdomain.Receipt#Receipt(java.util.List, double, double)}.
	 */
	@Test
	public void testReceipt()
	{
		assertEquals(2, receipt.getTotalNumberOfItems());
		assertEquals(9.00, receipt.getTotalSalesTax(), 0.0009);
		assertEquals(45.00, receipt.getTotalAmount(), 0.0009);
	}

}
