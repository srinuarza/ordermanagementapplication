package org.teksystems.ordermanagement.app.ordermanagement;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ OrderManagerTest.class, ShoppingCartTest.class,
		ShopStoreTest.class })
public class OrderManagementTestSuite {

}
