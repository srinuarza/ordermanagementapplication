/**
 * 
 */
package org.teksystems.ordermanagement.app.ordermanagement;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test case for {@link org.teksystems.ordermanagement.shopdomain.ShoppingStore}
 * 
 * @author srinivas
 */
public class OrderManagerTest {

	/** Test fixture */
	OrderManager order1 = new OrderManager("Order1");
	
	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.shopdomain.ShoppingStore#retrieveOrderAndPlaceInCart(java.util.List)}.
	 */
	@Test
	public void testRetrieveOrderAndPlaceInCart() 
	{
		String name = "box of chocolates";
		double price = 45.00;
		boolean imported = true;
		int quantity = 2;
		order1.retrieveItemAndPlaceInCart(name, price, imported, quantity);
		assertEquals(1, order1.getCart().getCartSize());
	}

	
}
