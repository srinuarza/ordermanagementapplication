/**
 * 
 */
package org.teksystems.ordermanagement.app.ordermanagement;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.teksystems.ordermanagement.model.items.BookItem;
import org.teksystems.ordermanagement.model.items.BookItemTest;
import org.teksystems.ordermanagement.model.items.Item;

/**
 * Test case for {@link org.teksystems.ordermanagement.shopdomain.ShoppingCart}
 *
 * @author srinivas
 */
public class ShoppingCartTest {

	
	/** Creates a test fixture. */
	ShoppingCart cart;
	Item Item;
	
	/**
	 * Initializes the fixture.
	 */
	@Before
	public void setUp()
	{
		cart = new ShoppingCart();
		Item = new BookItem();
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.shopdomain.ShoppingCart#addItemToCart(org.teksystems.ordermanagement.Items.Item)}.
	 */
	@Test
	public void testAddItemToCart() 
	{
		cart.addItemToCart(Item);
		assertEquals(1, cart.getCartSize());
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.shopdomain.ShoppingCart#getItemsFromCart()}.
	 */
	@Test
	public void testGetItemsFromCart() 
	{
		cart.addItemToCart(Item);
		assertEquals(Item ,cart.getItemsFromCart().get(0));
		BookItemTest test = new BookItemTest();
		test.testBookItem();
		
	}

}
