/**
 * 
 */
package org.teksystems.ordermanagement.app.ordermanagement.billing;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.teksystems.ordermanagement.app.ordermanagement.ShoppingCart;
import org.teksystems.ordermanagement.model.items.BookItem;
import org.teksystems.ordermanagement.model.items.FoodItem;
import org.teksystems.ordermanagement.model.items.MiscellaneousItem;

/**
 * Test case for {@link org.teksystems.ordermanagement.billingdomain.PaymentCounter}
 * 
 * @author srinivas
 */
public class PaymentCounterTest 
{

	/** Creates a test fixture */
	BookItem book;
	FoodItem food;
	MiscellaneousItem misc;
	PaymentCounter pay;
	ShoppingCart cart;
	
	/** Initializes the test fixture */
	@Before
	public void setUp()
	{
		cart = new ShoppingCart();
		book = new BookItem("book", 40.00, true, 1);
		food = new FoodItem("box of chocolates", 30.00, false, 1);
		misc = new MiscellaneousItem("bottle of perfume", 89.00, false, 1);
		cart.addItemToCart(book);
		cart.addItemToCart(food);
		cart.addItemToCart(misc);
		pay = new PaymentCounter();
	}
	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.billingdomain.PaymentCounter#billItemsInCart()}.
	 */
	@Test
	public void testBillItemsInCart() 
	{
		pay.billItemsInCart(cart);
		assertEquals(46.00, book.getTaxedCost(), 0.0009);
		assertEquals(33.00, food.getTaxedCost(), 0.0009);
		assertEquals(97.9, misc.getTaxedCost(), 0.0009);
		
	}
	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.billingdomain.PaymentCounter#getReceipt()}.
	 */
	@Test
	public void testPrintReceipt()
	{
		pay.billItemsInCart(cart);
		Receipt receipt = pay.getReceipt();
		pay.printReceipt(receipt);
	}

}
