/**
 * 
 */
package org.teksystems.ordermanagement.app.ordermanagement;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.teksystems.ordermanagement.model.items.FoodItem;
import org.teksystems.ordermanagement.model.items.FoodItemTest;
import org.teksystems.ordermanagement.model.items.Item;


/**
 * Test case for {@link com.ShopStore.shopdomain.Storestore}
 * 
 * @author srinivas
 */
public class ShopStoreTest {

	/** Creates a test fixture */
	ShopStore store = new ShopStore();
	String name = "box of chocolates";
	double price = 45.00;
	boolean imported = true;
	int quantity = 2;
	
	
	/**
	 * Test method for {@link com.ShopStore.shopdomain.Storestore#Storestore()}.
	 */
	@Test
	public void testStorestore() 
	{
		assertEquals(6, store.getstoreSize());
	}

	/**
	 * Test method for {@link com.ShopStore.shopdomain.Storestore#additemsTostore(java.lang.String, org.teksystems.ordermanagement.Items.Item)}.
	 */
	@Test
	public void testAdditemsTostore() 
	{
		store.additemsTostore("juice bottle", new FoodItem());
		assertEquals(7, store.getstoreSize());
		FoodItemTest test = new FoodItemTest();
		test.testFoodItem();
	}

	/**
	 * Test method for {@link com.ShopStore.shopdomain.Storestore#searchAndRetrieveItemFromstore(java.util.List)}.
	 */
	@Test
	public void testSearchAndRetrieveItemFromstore() 
	{
		Item Item = store.searchAndRetrieveItemFromstore(name, price, imported, quantity);
		assertEquals("box of chocolates", Item.getName());
		assertEquals(90.00, Item.getPrice(), 0.0009);
		assertEquals(true, Item.isImported());
		assertEquals(2, Item.getQuantity());
	}

}
