/**
 * 
 */
package org.teksystems.ordermanagement.app.ordermanagement.billing;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.teksystems.ordermanagement.app.utils.tax.TaxCalculator;
import org.teksystems.ordermanagement.model.items.BookItem;
import org.teksystems.ordermanagement.model.items.FoodItem;
import org.teksystems.ordermanagement.model.items.Item;

/**
 * Test case for {@link org.teksystems.ordermanagement.billingdomain.Biller}
 * 
 * @author srinivas
 */
public class BillerTest {

	/** Creates a test fixture */
	Biller b;
	List<Item> prodList;
	BookItem book;
	FoodItem food;
	
	/** Initializes the test fixture */
	@Before
	public void setUp()
	{
		b = new Biller(new TaxCalculator());
		book = new BookItem("book", 40.00, true, 1);
		food = new FoodItem("box of chocolates", 30.00, false, 1);
		book.setTaxedCost(45.00);
		food.setTaxedCost(37.00);
		prodList = new ArrayList<Item>();
		prodList.add(book);
		prodList.add(food);
	}
	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.billingdomain.Biller#calculateTax(double, double, boolean)}.
	 */
	@Test
	public void testCalculateTax() 
	{
		assertEquals(6.00, b.calculateTax(40.00, 0.10, true), 0.0009);
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.billingdomain.Biller#calcTotalProductCost(double, double)}.
	 */
	@Test
	public void testCalcTotalProductCost() 
	{
		assertEquals(46.00, b.calcTotalProductCost(40.00, 6.00), 0);
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.billingdomain.Biller#calcTotalTax(java.util.List)}.
	 */
	@Test
	public void testCalcTotalTax()
	{
		assertEquals(12, b.calcTotalTax(prodList), 0);
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.billingdomain.Biller#calcTotalAmount(java.util.List)}.
	 */
	@Test
	public void testCalcTotalAmount() 
	{
		assertEquals(70.0, b.calcTotalAmount(prodList), 0);
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.billingdomain.Biller#createNewReceipt(java.util.List, double, double)}.
	 */
	@Test
	public void testCreateNewReceipt()
	{
		Receipt r = b.createNewReceipt(prodList, 9.00, 45.00);
		assertEquals(2, r.getTotalNumberOfItems());
		assertEquals(9.00, r.getTotalSalesTax(), 0.0009);
		assertEquals(45.00, r.getTotalAmount(), 0.0009);
	}
	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.billingdomain.Biller#generateReceipt(Receipt))}.
	 */
	@Test
	public void testGenerateReceipt()
	{
		Receipt r = b.createNewReceipt(prodList, 9.00, 45.00);
		b.generateReceipt(r);
	}
}