package org.teksystems.ordermanagement;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.teksystems.ordermanagement.app.ordermanagement.OrderManagementTestSuite;
import org.teksystems.ordermanagement.app.ordermanagement.billing.BillingManagementTestSuite;
import org.teksystems.ordermanagement.app.utils.tax.TaxCalculationTestSuite;
import org.teksystems.ordermanagement.model.items.ItemsTestSuite;
import org.teksystems.ordermanagement.model.itemsfactory.ItemFactoryTestSuite;

@RunWith(Suite.class)
@SuiteClasses({ OrderManagementTestSuite.class,
		BillingManagementTestSuite.class, TaxCalculationTestSuite.class,
		ItemsTestSuite.class, ItemFactoryTestSuite.class })
public class MasterTestSuite {

}
