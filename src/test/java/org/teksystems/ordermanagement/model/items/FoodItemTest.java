/**
 * 
 */
package org.teksystems.ordermanagement.model.items;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Test case for {@link org.teksystems.ordermanagement.Items.FoodItem)
 * 
 * @author srinivas
 */
public class FoodItemTest {

	Item p;
	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.FoodItem#FoodItem()}.
	 */
	@Test
	public void testFoodItem()
	{
		p = new FoodItem();
		assertEquals("", p.getName());
		assertEquals(0.0, p.getPrice(), 0.0009);
		assertEquals(false, p.isImported());
		assertEquals(0, p.getQuantity());
		assertEquals(0.0, p.getTaxedCost(), 0.0009);
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.FoodItem#FoodItem(java.lang.String, double, boolean, int)}.
	 */
	@Test
	public void testFoodItemParaConst()
	{
		p = new FoodItem("a box of chocolates", 95.00, true, 3);
		assertEquals("a box of chocolates", p.getName());
		assertEquals(285.0, p.getPrice(), 0.0009);
		assertEquals(true, p.isImported());
		assertEquals(3, p.getQuantity());
	}
	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.FoodItem#getTaxValue(String)}.
	 */
	@Test
	public void testGetTaxValue()
	{
		p = new FoodItem("a box of chocolates", 95.00, true, 3);
		assertEquals(0.1, p.getTaxValue(), 0.0009);
	}

}
