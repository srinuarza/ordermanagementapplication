/**
 * 
 */
package org.teksystems.ordermanagement.model.items;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Test case for {@link org.teksystems.ordermanagement.Items.MiscellaneousItem)
 * 
 * @author srinivas
 */
public class MiscellaneousItemTest {

	Item p;
	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.MiscellaneousItem#MiscellaneousItem()}.
	 */
	@Test
	public void testMiscellaneousItem() 
	{
		p = new MiscellaneousItem();
		assertEquals("", p.getName());
		assertEquals(0.0, p.getPrice(), 0.0009);
		assertEquals(false, p.isImported());
		assertEquals(0, p.getQuantity());
		assertEquals(0.0, p.getTaxedCost(), 0.0009);
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.MiscellaneousItem#MiscellaneousItem(java.lang.String, double, boolean, int)}.
	 */
	@Test
	public void testMiscellaneousItemStringDoubleBooleanInt()
	{
		p = new MiscellaneousItem("a bottle of perfume", 95.00, true, 3);
		assertEquals("a bottle of perfume", p.getName());
		assertEquals(285.0, p.getPrice(), 0.0009);
		assertEquals(true, p.isImported());
		assertEquals(3, p.getQuantity());
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.MiscellaneousItem#getTaxValue(String)}.
	 */
	@Test
	public void testGetTaxValue()
	{
		p = new MiscellaneousItem("a bottle of perfume", 95.00, true, 3);
		assertEquals(0.10, p.getTaxValue(), 0.0009);
	}
}
