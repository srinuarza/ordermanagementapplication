package org.teksystems.ordermanagement.model.items;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ItemTest.class ,BookItemTest.class, FoodItemTest.class,
		 MedicalItemTest.class,MiscellaneousItemTest.class })
public class ItemsTestSuite {

}
