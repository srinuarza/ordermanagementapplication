/**
 * 
 */
package org.teksystems.ordermanagement.model.items;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


/**
 * Test case for {@link org.teksystems.ordermanagement.Items.Item)
 *
 * @author srinivas
 */
public class ItemTest {

	/** Creates a test fixture. */
	Item Item;
	
	/**
	 * Initialize the fixture by instantiating the mock object.
	 */
	@Before
	public void setUp()
	{
		Item = new MockItem("Mock", 50.00, true, 2);
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.Item#getName()}.
	 */
	@Test
	public void testGetName() 
	{
	
		assertEquals("Mock", Item.getName());
	}

	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.Item#getPrice()}.
	 */
	@Test
	public void testGetPrice() 
	{
		assertEquals(100.00, Item.getPrice(), 0.0009);
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.Item#isImported()}.
	 */
	@Test
	public void testIsImported() 
	{
		assertEquals(true, Item.isImported());
	}


	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.Item#getQuantity()}.
	 */
	@Test
	public void testGetQuantity() 
	{
		assertEquals(2, Item.getQuantity());
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.Item#getTaxedCost()}.
	 */
	@Test
	public void testGetTaxedCost() 
	{
		assertEquals(0.0, Item.getTaxedCost(), 0.0009);
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.Item#toString()}.
	 */
	@Test
	public void testToString()
	{
		assertEquals("2 imported Mock : 0.0", Item.toString());
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.Item#toString(boolean)}.
	 */
	@Test
	public void testToStringBoolean()
	{
		assertEquals("imported", Item.toString(Item.isImported()));
	}

}
