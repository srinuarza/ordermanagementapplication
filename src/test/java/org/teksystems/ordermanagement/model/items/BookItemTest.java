/**
 * 
 */
package org.teksystems.ordermanagement.model.items;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Test case for {@link org.teksystems.ordermanagement.Items.BookItem)
 * 
 * @author srinivas
 */
public class BookItemTest {

	Item p;
	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.BookItem#BookItem()}.
	 */
	@Test
	public void testBookItem() 
	{
		p = new BookItem();
		assertEquals("", p.getName());
		assertEquals(0.0, p.getPrice(), 0.0009);
		assertEquals(false, p.isImported());
		assertEquals(0, p.getQuantity());
		assertEquals(0.0, p.getTaxedCost(), 0.0009);
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.BookItem#BookItem(java.lang.String, double, boolean, int)}.
	 */
	@Test
	public void testBookItemParaConst()
	{
	
		p = new BookItem("book", 95.00, true, 3);
		assertEquals("book", p.getName());
		assertEquals(285.0, p.getPrice(), 0.0009);
		assertEquals(true, p.isImported());
		assertEquals(3, p.getQuantity());
	}

	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.BookItem#getTaxValue(String)}.
	 */
	@Test
	public void testGetTaxValue()
	{
		p = new BookItem("book", 95.00, true, 3);
		assertEquals(0.1, p.getTaxValue(), 0.0009);
	}
}
