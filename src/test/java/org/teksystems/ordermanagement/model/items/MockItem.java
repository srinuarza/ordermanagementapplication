/**
 * 
 */
package org.teksystems.ordermanagement.model.items;

import org.teksystems.ordermanagement.model.itemsfactory.ItemFactory;

/** MockItem is a mock object used to test the abstract Item class.
 * 
 * @author user
 *
 */
public class MockItem extends Item {

	/**
	 * Default constructor to initialize a new mock Item.
	 */
	public MockItem() 
	{
		super();
	}

	/**
	 * Parameterized constructor to initialize the values of the <code>MockItem</code> attributes.
	 *
	 * @param name - the name of the mock Item
	 * @param price - the price of the mock Item
	 * @param imported - boolean value of whether the mock Item imported or not
	 * @param quantity - quantity of mock Item items
	 */	
	public MockItem(String name, double price, boolean imported, int quantity)
	{
		super(name, price, imported, quantity);
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ItemFactory getFactory() 
	{	
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getTaxValue() 
	{
		return 0;
	}

}
