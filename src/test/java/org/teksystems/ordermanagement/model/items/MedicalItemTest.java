/**
 * 
 */
package org.teksystems.ordermanagement.model.items;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Test case for {@link org.teksystems.ordermanagement.Items.MedicalItem)
 * 
 * @author srinivas
 */
public class MedicalItemTest {

	Item p;
	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.MedicalItem#MedicalItem()}.
	 */
	@Test
	public void testMedicalItem() 
	{
		p = new MedicalItem();
		assertEquals("", p.getName());
		assertEquals(0.0, p.getPrice(), 0.0009);
		assertEquals(false, p.isImported());
		assertEquals(0, p.getQuantity());
		assertEquals(0.0, p.getTaxedCost(), 0.0009);
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.MedicalItem#MedicalItem(java.lang.String, double, boolean, int)}.
	 */
	@Test
	public void testMedicalItemParaConst() 
	{
		p = new MedicalItem("a packet of headache pills", 95.00, true, 3);
		assertEquals("a packet of headache pills", p.getName());
		assertEquals(285.0, p.getPrice(), 0.0009);
		assertEquals(true, p.isImported());
		assertEquals(3, p.getQuantity());
	}
	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.Items.MedicalItem#getTaxValue(String)}.
	 */
	@Test
	public void testGetTaxValue()
	{
		p = new MedicalItem("a packet of headache pills", 95.00, true, 3);
		assertEquals(0.1, p.getTaxValue(), 0.0009);
	}

}
