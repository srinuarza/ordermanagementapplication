/**
 * 
 */
package org.teksystems.ordermanagement.model.itemsfactory;

import org.junit.Test;

/**
 * The ItemFactoriesTest tests the Item factory classes.
 * 
 * @author srinivas
 */
public class ItemFactoriesTest {

	/**Creates a test fixture */
	ItemFactory factory;
	String name = "mock";
	double price = 45.00;
	boolean imported = true;
	int quantity = 2;
	
	/**
	 * Test method for {@link org.teksystems.ordermanagement.Itemfactories.BookItemFactory#createItem(java.util.List)}.
	 */
	@Test
	public void testCreateBookItem() 
	{
		factory = new BookItemFactory();
		factory.createItem(name, price, imported, quantity);
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.Itemfactories.FoodItemFactory#createItem(java.util.List)}.
	 */
	@Test
	public void testCreateFoodItem() 
	{
		factory = new FoodItemFactory();
		factory.createItem(name, price, imported, quantity);
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.Itemfactories.MedicalItemFactory#createItem(java.util.List)}.
	 */
	@Test
	public void testCreateMedicalItem() 
	{
		factory = new MedicalItemFactory();
		factory.createItem(name, price, imported, quantity);
	}

	/**
	 * Test method for {@link org.teksystems.ordermanagement.Itemfactories.MiscellaneousItemFactory#createItem(java.util.List)}.
	 */
	@Test
	public void testCreateMiscellaneousItem() 
	{
		factory = new MiscellaneousItemFactory();
		factory.createItem(name, price, imported, quantity);
	}

	
}
