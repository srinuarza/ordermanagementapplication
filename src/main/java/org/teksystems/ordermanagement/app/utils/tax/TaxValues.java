/**
 * 
 */
package org.teksystems.ordermanagement.app.utils.tax;

/**
 * The Enum TaxValues contains tax rates of different products
 *
 * @author srinivas
 */
public enum TaxValues {

	/** The tax values of various products. */
	BOOK_TAX(0.10), FOOD_TAX(0.10), MEDICAL_TAX(0.10), MISC_TAX(0.10);

	/** The item tax value. */
	private double itemTaxValue;

	/**
	 * Instantiates a new LocalTaxValues item.
	 *
	 * @param taxValue
	 *            - the tax rate of the product item.
	 */
	private TaxValues(double taxValue) {
		itemTaxValue = taxValue;
	}

	public double getTax() {
		return itemTaxValue;
	}

}
