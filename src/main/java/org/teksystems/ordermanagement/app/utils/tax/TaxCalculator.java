/**
 * 
 */
package org.teksystems.ordermanagement.app.utils.tax;

/**
 * The TaxCalculator calculates the total tax cost.
 * 
 * @author srinivas
 */
public class TaxCalculator {

	public double calculateTax(double price, double localTax, boolean imported) {
		// Calculates tax cost
		double tax = price * localTax;

		// Calculates imported duty
		if (imported == true) {
			tax += (price * 0.05);
		}

		// rounds off to nearest 0.05 paise
		tax = MathHelper.roundoff(tax);

		return tax;
	}

}
