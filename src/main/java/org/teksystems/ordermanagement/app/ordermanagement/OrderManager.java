/**
 * 
 */
package org.teksystems.ordermanagement.app.ordermanagement;

import java.util.Scanner;

import org.teksystems.ordermanagement.app.ordermanagement.billing.PaymentCounter;
import org.teksystems.ordermanagement.app.ordermanagement.billing.Receipt;
import org.teksystems.ordermanagement.model.items.Item;

/**
 * The OrderManager class deals with getting a order and placing in shopping
 * cart and billing the items in the cart.
 * 
 * @author srinivas
 */
public class OrderManager {

	private String orderName;

	/** The cart holds the purchased items to be billed. */
	private ShoppingCart shoppingCart;

	/** The storeshelf contains the items of the store. */
	private ShopStore shopStore;

	/** The payment counter performs billing and generates receipt. */
	private PaymentCounter paymentCounter;

	/** gets user input of items. */
	private Scanner input;

	/**
	 * Instantiates a new shopping store containing a shopping cart, storeshelf
	 * and payment counter.
	 * 
	 * @param orderName
	 */
	public OrderManager(String orderName) {
		this.orderName = orderName;
		shoppingCart = new ShoppingCart();
		shopStore = new ShopStore();
		paymentCounter = new PaymentCounter();
		input = new Scanner(System.in);
	}

	/**
	 * Gets the orders List.
	 *
	 * @return the order
	 */
	public void addOrder1ItemsToCart() {

		retrieveItemAndPlaceInCart("book", 12.49, false, 1);
		retrieveItemAndPlaceInCart("music CD", 14.99, false, 1);
		retrieveItemAndPlaceInCart("chocolate bar", 0.85, false, 1);

	}

	/**
	 * Gets the orders List.
	 *
	 * @return the order
	 */
	public void addOrder2ItemsToCart() {

		retrieveItemAndPlaceInCart("box of chocolates", 10, true, 1);
		retrieveItemAndPlaceInCart("bottle of perfume", 47.50, true, 1);

	}

	/**
	 * Gets the orders List.
	 *
	 * @return the order
	 */
	public void addOrder3ItemsToCart() {

		retrieveItemAndPlaceInCart("bottle of perfume", 27.99, true, 1);
		retrieveItemAndPlaceInCart("bottle of perfume", 18.99, false, 1);
		retrieveItemAndPlaceInCart("packet of headache pills", 9.75, false, 1);
		retrieveItemAndPlaceInCart("box of chocolates", 11.25, true, 1);

	}

	/**
	 * Gets the orders List.
	 *
	 * @return the order
	 */
	public void addOrdersToCartFromCommandLine() {
		do {
			String name = getitemName();
			double price = getitemPrice();
			boolean imported = isitemImported();
			int quantity = getQuantity();
			retrieveItemAndPlaceInCart(name, price, imported, quantity);

		} while (isAddAnotheritem());
	}

	/**
	 * Retrieves order items and places in cart.
	 *
	 * @param name
	 *            - name of the item
	 * @param price
	 *            - price of item
	 * @param imported
	 *            - boolean value of whether imported or not
	 * @param quantity
	 *            - quantity of item
	 * @return
	 */
	public void retrieveItemAndPlaceInCart(String name, double price,
			boolean imported, int quantity) {
		Item item = shopStore.searchAndRetrieveItemFromstore(name, price, imported,
				quantity);
		if (item != null) {
			shoppingCart.addItemToCart(item);
		} else {
			System.exit(-1);

		}
	}

	/**
	 * Performs billing operation of purchased items and generates receipt.
	 */
	public void checkOut() {
		paymentCounter.billItemsInCart(shoppingCart);
		Receipt receipt = paymentCounter.getReceipt();
		System.out.println("********" + orderName + "*********");
		paymentCounter.printReceipt(receipt);
	}

	/**
	 * Gets the item name from the user.
	 *
	 * @return String
	 */
	public String getitemName() {
		System.out.println("Enter the item name:\n");
		return input.nextLine();
	}

	/**
	 * Gets the item price from the user.
	 *
	 * @return double
	 */
	public double getitemPrice() {
		System.out.println("Enter the item price:\n");

		// validates user input
		while (!(input.hasNextDouble())) {
			System.out.println("Invalid price. Enter a number");
		}
		return input.nextDouble();
	}

	/**
	 * Asks the user whether item imported.
	 *
	 * @return <code>true</code>, if item imported, else <code>false</code>
	 */
	public boolean isitemImported() {
		System.out.println("Is item imported or not?(Y/N)\n");

		// validates user input
		while (!(input.hasNext())) {
			System.out.println("Invalid input. Enter (Y/N)");
		}

		char ans = input.next().charAt(0);

		// parses char value of 'imported' into a boolean value
		boolean imported = parseBoolean(ans);
		return imported;
	}

	/**
	 * Gets the quantity of item from user.
	 *
	 * @return int
	 */
	public int getQuantity() {
		System.out.println("Enter the quantity:\n");

		// validates user input
		while (!(input.hasNextInt())) {
			System.out.println("Invalid input. Enter a integer");
		}
		return input.nextInt();
	}

	/**
	 * Asks the user whether to add another item.
	 *
	 * @return <code>true</code>, if another item adds, else <code>false</code>
	 */
	public boolean isAddAnotheritem() {
		System.out.println("Do you want to add another item?(Y/N)");

		// validates user input
		while (!(input.hasNext())) {
			System.out.println("Invalid input. Enter (Y/N)");
		}

		char ans = input.next().charAt(0);

		// parses the answer into a boolean value
		boolean addAnotheritem = parseBoolean(ans);

		// moves scanner to next line to get next input value
		input.nextLine();
		return addAnotheritem;
	}

	/**
	 * Parses the character value of 'Y' or 'N' into boolean value.
	 *
	 * @param value
	 *            - the character to be parsed
	 * @return <code>true</code>, if value is 'Y', else <code>false</code>
	 */
	public boolean parseBoolean(char value) {
		boolean flag = true;
		boolean boolValue = false;

		while (flag) {
			// parses 'Y' into 'true'
			if (value == 'Y' || value == 'y') {
				boolValue = true;
				flag = false;
			}

			// parses 'N' into 'false'
			else if (value == 'N' || value == 'n') {
				boolValue = false;
				flag = false;
			}

			// validates user input
			else {
				System.out.println("Invalid input. Enter (Y/N)");
			}
		}

		return boolValue;
	}

	/**
	 * gets the cart containing item items.
	 * 
	 * @return ShoppingCart
	 */
	public ShoppingCart getCart() {
		return shoppingCart;
	}

	public PaymentCounter getPaymentCounter() {
		return paymentCounter;
	}
}
