/**
 * 
 */
package org.teksystems.ordermanagement.app.ordermanagement;

import java.util.ArrayList;
import java.util.List;

import org.teksystems.ordermanagement.model.items.Item;

/**
 * The ShoppingCart holds the items purchased and from it the items are
 * retrieved for billing.
 * 
 * @author srinivas
 */
public class ShoppingCart {

	/** A list to store the items. */
	private List<Item> itemList;

	/**
	 * Instantiates a new shopping cart to which items can be added.
	 */
	public ShoppingCart() {
		itemList = new ArrayList<Item>();
	}

	/**
	 * Adds the item to the cart.
	 *
	 * @param item
	 *            - the item to be added to the cart.
	 */
	public void addItemToCart(Item item) {
		itemList.add(item);
	}

	/**
	 * Retrieves the items from cart.
	 *
	 * @return a list containing items
	 */
	public List<Item> getItemsFromCart() {
		return itemList;
	}

	/**
	 * Gets the number of items in the cart.
	 *
	 * @return int
	 */
	public int getCartSize() {
		return itemList.size();
	}
}
