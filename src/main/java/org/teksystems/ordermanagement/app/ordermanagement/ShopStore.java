package org.teksystems.ordermanagement.app.ordermanagement;

import java.util.HashMap;

import org.teksystems.ordermanagement.model.items.BookItem;
import org.teksystems.ordermanagement.model.items.FoodItem;
import org.teksystems.ordermanagement.model.items.MedicalItem;
import org.teksystems.ordermanagement.model.items.MiscellaneousItem;
import org.teksystems.ordermanagement.model.items.Item;

/**
 * The Storestore stores all the different categories of item items of the
 * shopping store from which a item of a specific category can be retrieved for
 * billing.
 * 
 * @author srinivas
 */
public class ShopStore {

	/** The item items mapped to their respective categories */
	private HashMap<String, Item> items;

	/**
	 * Instantiates a new store store with the item items.
	 */
	public ShopStore() {
		items = new HashMap<String, Item>();
		additemsTostore("book", new BookItem());
		additemsTostore("music CD", new MiscellaneousItem());
		additemsTostore("chocolate bar", new FoodItem());
		additemsTostore("box of chocolates", new FoodItem());
		additemsTostore("bottle of perfume", new MiscellaneousItem());
		additemsTostore("packet of headache pills", new MedicalItem());

	}

	/**
	 * Adds the item items to store store in their respective categories.
	 *
	 * @param itemItem
	 *            - the item item
	 * @param itemCategory
	 *            - the item category to which a particular item belongs.
	 */
	public void additemsTostore(String itemItem, Item itemCategory) {
		items.put(itemItem, itemCategory);

	}

	/**
	 * Searches for the item using the string name and maps it to the item
	 * category to create a new item object.
	 *
	 * @param name
	 *            - name of the item
	 * @param price
	 *            - price of item
	 * @param imported
	 *            - boolean value of whether imported or not
	 * @param quantity
	 *            - quantity of item
	 * @return item
	 */
	public Item searchAndRetrieveItemFromstore(String name, double price,
			boolean imported, int quantity) {
		if (items.get(name) != null) {
			Item itemItem = items.get(name).getFactory()
					.createItem(name, price, imported, quantity);
			return itemItem;
		} else {
			System.out.println("item " + name
					+ " Not Found in Shop. Please enter valid item Name."
					+ "\n The available items in the shop are : +\n"
					+ items.keySet());
		}
		return null;
	}

	/**
	 * gets the total number of items present in store.
	 * 
	 * @return int
	 */
	public int getstoreSize() {
		return items.size();
	}
}
