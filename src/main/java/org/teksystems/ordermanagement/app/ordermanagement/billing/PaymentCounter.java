/**
 * 
 */
package org.teksystems.ordermanagement.app.ordermanagement.billing;

import java.util.List;

import org.teksystems.ordermanagement.app.ordermanagement.ShoppingCart;
import org.teksystems.ordermanagement.app.utils.tax.TaxCalculator;
import org.teksystems.ordermanagement.model.items.Item;

// TODO: Auto-generated Javadoc
/**
 * The PaymentCounter contains a Biller which performs billing of items in the
 * cart and generates the receipt.
 * 
 * @author srinivas
 */
public class PaymentCounter {

	private Biller biller;
	private Receipt receipt;
	private List<Item> productList;

	/**
	 * Instantiates a new payment counter.
	 */
	public PaymentCounter() {
	}

	/**
	 * Bills all the items in the shopping cart.
	 * 
	 * @param cart
	 *            - the cart containing the items to be billed.
	 */
	public void billItemsInCart(ShoppingCart cart) {
		// retrieves all the products from cart
		productList = cart.getItemsFromCart();

		for (Item p : productList) {
			biller = getBiller();

			// computes the total tax of product by a regional tax rates
			double productTax = biller.calculateTax(p.getPrice(),
					p.getTaxValue(), p.isImported());

			// computes the total taxedcost of the product
			double taxedCost = biller.calcTotalProductCost(p.getPrice(),
					productTax);

			// sets the taxed cost of the product
			p.setTaxedCost(taxedCost);
		}

	}

	/**
	 * Generates a new receipt for the products purchased with their taxes and
	 * net payable amount.
	 * 
	 * @return Receipt
	 */
	public Receipt getReceipt() {
		double totalTax = biller.calcTotalTax(productList);
		double totalAmount = biller.calcTotalAmount(productList);
		receipt = biller.createNewReceipt(productList, totalTax, totalAmount);
		return receipt;
	}

	/**
	 * Prints out the receipt
	 * 
	 * @param - the receipt to be printed
	 */
	public void printReceipt(Receipt receipt) {
		biller.generateReceipt(receipt);
	}

	/**
	 * Gets the biller object to perform billing operations.
	 *
	 * @param strategy
	 *            - the regional strategy for tax calculations
	 * @return Biller
	 */
	public Biller getBiller() {
		TaxCalculator taxCal = new TaxCalculator();
		return new Biller(taxCal);
	}

}
