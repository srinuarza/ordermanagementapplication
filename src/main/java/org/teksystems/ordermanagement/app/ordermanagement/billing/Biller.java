/**
 * 
 */
package org.teksystems.ordermanagement.app.ordermanagement.billing;

import java.util.List;

import org.teksystems.ordermanagement.app.utils.tax.MathHelper;
import org.teksystems.ordermanagement.app.utils.tax.TaxCalculator;
import org.teksystems.ordermanagement.model.items.Item;

/**
 * The Biller computes the product's taxed cost, the total tax value, and total
 * amount of all products.
 * 
 * @author srinivas
 */
public class Biller {

	/** The tax calculator. */
	TaxCalculator taxCalculator;

	/**
	 * Instantiates a new biller.
	 *
	 * @param taxCalc
	 *            - the tax calculator
	 */
	public Biller(TaxCalculator taxCalc) {
		taxCalculator = taxCalc;
	}

	/**
	 * Calculates the total product tax using tax calculator.
	 * 
	 * @param price
	 *            - the price of the product
	 * @param tax
	 *            - the product's tax rate
	 * @param imported
	 *            - boolean value of whether product imported or not
	 * @return double
	 */
	public double calculateTax(double price, double tax, boolean imported) {

		double totalProductTax = taxCalculator.calculateTax(price, tax,
				imported);
		return totalProductTax;
	}

	/**
	 * Calculates the taxed cost of the product which is the sum of the original
	 * price and tax value.
	 * 
	 * @param price
	 *            - the price of the product
	 * @param tax
	 *            - the product's tax value
	 * @return double
	 */
	public double calcTotalProductCost(double price, double tax) {
		return MathHelper.truncate(price + tax);
	}

	/**
	 * Computes the net tax values of all products.
	 * 
	 * @param prodList
	 *            - the list of all products
	 * @return double
	 */
	public double calcTotalTax(List<Item> prodList) {
		double totalTax = 0.0;

		// Calculates the tax value which is the difference between the taxed
		// cost and original cost.
		for (Item p : prodList) {
			totalTax += (p.getTaxedCost() - p.getPrice());
		}

		return MathHelper.truncate(totalTax);
	}

	/**
	 * Computes the net amount of all the products.
	 * 
	 * @param prodList
	 *            - the list of all products
	 * @return double
	 */
	public double calcTotalAmount(List<Item> prodList) {
		double totalAmount = 0.0;

		// Sums the total cost of all products.
		for (Item p : prodList) {
			totalAmount += p.getPrice();
		}

		return MathHelper.truncate(totalAmount);
	}

	/**
	 * Creates a new Receipt object.
	 * 
	 * @param productList
	 *            - the products purchased
	 * @param totalTax
	 *            - the net tax cost
	 * @param totalAmount
	 *            - the net payable amount
	 * @return Receipt
	 */
	public Receipt createNewReceipt(List<Item> productList, double totalTax,
			double totalAmount) {
		return new Receipt(productList, totalTax, totalAmount);
	}

	/**
	 * Prints out the receipt.
	 * 
	 * @param r
	 *            - the Receipt to be printed
	 */
	public void generateReceipt(Receipt r) {
		String receipt = r.toString();
		System.out.println(receipt);
	}
}
