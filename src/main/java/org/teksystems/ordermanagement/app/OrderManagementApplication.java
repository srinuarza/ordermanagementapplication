/**
 * 
 */
package org.teksystems.ordermanagement.app;

import org.teksystems.ordermanagement.app.ordermanagement.OrderManager;

/**
 * The Main Program that drives the Order management project
 * 
 * @author srinivas
 */
public class OrderManagementApplication {

	/**
	 * Main method
	 */
	public static void main(String[] args) {

		double grossTotalAmount = 0;
		// Place Order1
		OrderManager order1 = new OrderManager("Order1");
		order1.addOrder1ItemsToCart();
		order1.checkOut();
		grossTotalAmount = grossTotalAmount
				+ order1.getPaymentCounter().getReceipt().getTotalAmount();
		// Place Order2
		OrderManager order2 = new OrderManager("Order2");
		order2.addOrder2ItemsToCart();
		order2.checkOut();
		grossTotalAmount = grossTotalAmount
				+ order2.getPaymentCounter().getReceipt().getTotalAmount();

		// Place Order2
		OrderManager order3 = new OrderManager("Order3");
		order3.addOrder3ItemsToCart();
		order3.checkOut();
		grossTotalAmount = grossTotalAmount
				+ order3.getPaymentCounter().getReceipt().getTotalAmount();

		System.out.println("Sum of All Orders :" + grossTotalAmount);

	}

}
