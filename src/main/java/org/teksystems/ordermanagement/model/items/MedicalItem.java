/**
 * 
 */
package org.teksystems.ordermanagement.model.items;

import org.teksystems.ordermanagement.app.utils.tax.TaxValues;
import org.teksystems.ordermanagement.model.itemsfactory.MedicalItemFactory;

/**
 * MedicalItem is an item belonging to the category 'Medical' in the shopping
 * store.
 *
 * @author srinivas
 */
public class MedicalItem extends Item {

	/**
	 * Default constructor to initialize a new medical product.
	 */
	public MedicalItem() {
		super();
	}

	/**
	 * Parameterized constructor to initialize the values of the
	 * <code>MedicalProduct</code> attributes.
	 *
	 * @param name
	 *            - the name of the medical product
	 * @param price
	 *            - the price of the medical product
	 * @param imported
	 *            - boolean value of whether the medical product imported or not
	 * @param quantity
	 *            - quantity of medical product items
	 */
	public MedicalItem(String name, double price, boolean imported, int quantity) {
		super(name, price, imported, quantity);

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return MedicalProductFactory
	 */
	@Override
	public MedicalItemFactory getFactory() {
		return new MedicalItemFactory();

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getTaxValue() {

		return TaxValues.MEDICAL_TAX.getTax();

	}

}
