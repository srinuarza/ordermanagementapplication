/**
 * 
 */
package org.teksystems.ordermanagement.model.items;

import org.teksystems.ordermanagement.app.utils.tax.TaxValues;
import org.teksystems.ordermanagement.model.itemsfactory.FoodItemFactory;

/**
 * FoodItem is an item belonging to the category 'Food' in the shopping store.
 *
 * @author srinivas
 */
public class FoodItem extends Item {

	/**
	 * Default constructor to initialize a new food Item.
	 */
	public FoodItem() {
		super();
	}

	/**
	 * Parameterized constructor to initialize the values of the
	 * <code>FoodItem</code> attributes.
	 *
	 * @param name
	 *            - the name of the food Item
	 * @param price
	 *            - the price of the food Item
	 * @param imported
	 *            - boolean value of whether the food Item imported or not
	 * @param quantity
	 *            - quantity of food Item items
	 */
	public FoodItem(String name, double price, boolean imported, int quantity) {
		super(name, price, imported, quantity);

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return FoodItemFactory
	 */
	@Override
	public FoodItemFactory getFactory() {

		return new FoodItemFactory();

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getTaxValue() {
		return TaxValues.FOOD_TAX.getTax();

	}

}
