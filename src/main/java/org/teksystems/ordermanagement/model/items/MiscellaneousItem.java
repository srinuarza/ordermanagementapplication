/**
 * 
 */
package org.teksystems.ordermanagement.model.items;

import org.teksystems.ordermanagement.app.utils.tax.TaxValues;
import org.teksystems.ordermanagement.model.itemsfactory.MiscellaneousItemFactory;

/**
 * MiscellaneousItem is an item belonging to the category 'Miscellaneous' in the
 * shopping store.
 *
 * @author srinivas
 */
public class MiscellaneousItem extends Item {

	/**
	 * Default constructor to initialize a new miscellaneous Item.
	 */
	public MiscellaneousItem() {
		super();
	}

	/**
	 * Parameterized constructor to initialize the values of the
	 * <code>MiscellaneousItem</code> attributes.
	 *
	 * @param name
	 *            - the name of the miscellaneous Item
	 * @param price
	 *            - the price of the miscellaneous Item
	 * @param imported
	 *            - boolean value of whether the miscellaneous Item imported or
	 *            not
	 * @param quantity
	 *            - quantity of miscellaneous Item items
	 */
	public MiscellaneousItem(String name, double price, boolean imported,
			int quantity) {
		super(name, price, imported, quantity);

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return MiscellaneousItemFactory
	 */
	public MiscellaneousItemFactory getFactory() {

		return new MiscellaneousItemFactory();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getTaxValue() {
		return TaxValues.MISC_TAX.getTax();

	}
}
