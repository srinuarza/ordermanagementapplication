/**
 * 
 */
package org.teksystems.ordermanagement.model.items;

import org.teksystems.ordermanagement.app.utils.tax.TaxValues;
import org.teksystems.ordermanagement.model.itemsfactory.BookItemFactory;

/**
 * BookItem is an item belonging to the category 'Book' in the shopping store.
 *
 * @author srinivas
 */
public class BookItem extends Item {

	/**
	 * Default constructor to initialize a new book Item.
	 */
	public BookItem() {
		super();
	}

	/**
	 * Parameterized constructor to initialize the values of the
	 * <code>BookItem</code> attributes.
	 *
	 * @param name
	 *            - the name of the book Item
	 * @param price
	 *            - the price of the book Item
	 * @param imported
	 *            - boolean value of whether the book Item imported or not
	 * @param quantity
	 *            - quantity of book Item items
	 */
	public BookItem(String name, double price, boolean imported, int quantity) {
		super(name, price, imported, quantity);

	}

	/**
	 * {@inheritDoc}
	 * 
	 * @return BookItemFactory
	 */
	@Override
	public BookItemFactory getFactory() {
		return new BookItemFactory();

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getTaxValue() {
		return TaxValues.BOOK_TAX.getTax();
	}
}
