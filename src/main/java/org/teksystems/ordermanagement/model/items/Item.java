/**
 * 
 */
package org.teksystems.ordermanagement.model.items;

import org.teksystems.ordermanagement.model.itemsfactory.ItemFactory;

/**
 * The Item is the item which is purchased in the shopping store and for which a
 * receipt is generated.
 *
 * @author srinivas
 * 
 */
public abstract class Item {

	/** name - name of the Item. */
	protected String name;

	/**
	 * price - original price of the Item excluding tax for the number of items
	 * specified in the quantity.
	 */
	protected double price;

	/**
	 * imported - boolean value which indicates whether Item is imported or not.
	 */
	protected Boolean imported;

	/** quantity - the quantity of the Item to be purchased. */
	protected int quantity;

	/** taxedCost - the net price of the Item including tax. */
	protected double taxedCost;

	/**
	 * Default constructor to instantiate Item.
	 */
	public Item() {
		this.name = "";
		this.price = 0.0;
		this.imported = false;
		this.quantity = 0;
		this.taxedCost = 0.0;
	}

	/**
	 * Parameterized constructor to initialize the values of the Item
	 * attributes.
	 *
	 * @param name
	 *            - the name of the Item
	 * @param price
	 *            - the price of the Item
	 * @param imported
	 *            - boolean value of whether Item imported or not
	 * @param quantity
	 *            - quantity of Item item
	 */
	public Item(String name, double price, boolean imported, int quantity) {
		this.name = name;
		this.price = price * quantity;
		this.imported = imported;
		this.quantity = quantity;
	}

	/**
	 * Gets the name of the Item
	 *
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the Item
	 *
	 * @param name
	 *            - the Item's name as a String
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the price of the Item.
	 *
	 * @return double
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * Calculates the price for the number of items mentioned in the quantity.
	 *
	 * @param price
	 *            - the Item's price as a double
	 */
	public void setPrice(double price) {
		this.price = price * quantity;
	}

	/**
	 * Checks if Item is imported.
	 *
	 * @return <code>true</code> if the Item is imported, otherwise
	 *         <code>false</code>
	 */
	public boolean isImported() {
		return imported;
	}

	/**
	 * Sets a boolean value for <code>imported</code> attribute
	 *
	 * @param imported
	 *            - a boolean value which is <code>true</code> if the Item is
	 *            imported, otherwise <code>false</code>
	 */
	public void setImported(boolean imported) {
		this.imported = imported;
	}

	/**
	 * Gets the quantity of the Item.
	 *
	 * @return int
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity of Item.
	 *
	 * @param quantity
	 *            - the Item's quantity as an int
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	/**
	 * Gets the net cost of the Item including tax.
	 *
	 * @return double
	 */
	public double getTaxedCost() {
		return taxedCost;
	}

	/**
	 * Sets the total taxed cost of the Item.
	 *
	 * @param taxedCost
	 *            - the Item's total taxed value as a double
	 */
	public void setTaxedCost(double taxedCost) {
		this.taxedCost = taxedCost;
	}

	/** @inheritDoc */
	@Override
	public String toString() {
		return (quantity + " " + toString(imported) + " " + name + " : " + taxedCost);
	}

	/**
	 * Gets a String representation for the <code>imported</code> attribute.
	 *
	 * @param imported
	 *            - boolean value for imported
	 * @return String
	 */
	public String toString(boolean imported) {
		if (imported == false) {
			return "";
		}

		else {
			return "imported";
		}
	}

	/**
	 * Gets the factory object to create a Item.
	 *
	 * @return ItemFactory
	 */
	public abstract ItemFactory getFactory();

	/**
	 * Gets the tax value of the Item in a particular region.
	 * 
	 * @return double
	 */
	public abstract double getTaxValue();
}
