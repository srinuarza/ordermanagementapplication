package org.teksystems.ordermanagement.model.itemsfactory;

import org.teksystems.ordermanagement.model.items.Item;

/**
 * The ItemFactory is an abstract factory class.
 * 
 * @author srinivas
 */
public abstract class ItemFactory {

	/**
	 * Creates a new Item object.
	 *
	 * @param name
	 *            - name of the item
	 * @param price
	 *            - price of item
	 * @param imported
	 *            - boolean value of whether imported or not
	 * @param quantity
	 *            - quantity of item
	 * @return item
	 */
	public abstract Item createItem(String name, double price,
			boolean imported, int quantity);

}
