package org.teksystems.ordermanagement.model.itemsfactory;

import org.teksystems.ordermanagement.model.items.MiscellaneousItem;

/**
 * The MiscellaneousItemFactory is a factory class to create a new
 * MiscellaneousItem object with specified properties.
 * 
 * @author srinivas
 */
public class MiscellaneousItemFactory extends ItemFactory {

	/**
	 * {@inheritDoc}
	 * 
	 * @return MiscellaneousItem
	 */
	@Override
	public MiscellaneousItem createItem(String name, double price,
			boolean imported, int quantity) {
		return new MiscellaneousItem(name, price, imported, quantity);
	}

}
