package org.teksystems.ordermanagement.model.itemsfactory;

import org.teksystems.ordermanagement.model.items.MedicalItem;

/**
 * The Medicali ItemFactory is a factory class to create a new Medicalitem
 * object with specified properties.
 * 
 * @author srinivas
 */
public class MedicalItemFactory extends ItemFactory {

	/**
	 * {@inheritDoc}
	 * 
	 * @return MedicalItem
	 */
	@Override
	public MedicalItem createItem(String name, double price, boolean imported,
			int quantity) {
		return new MedicalItem(name, price, imported, quantity);
	}

}
