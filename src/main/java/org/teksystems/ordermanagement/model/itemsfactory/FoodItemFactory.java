package org.teksystems.ordermanagement.model.itemsfactory;

import org.teksystems.ordermanagement.model.items.FoodItem;

/**
 * The FoodItemFactory is a factory class to create a new Fooditem object with
 * specified properties.
 * 
 * @author srinivas
 */
public class FoodItemFactory extends ItemFactory {

	/**
	 * {@inheritDoc}
	 * 
	 * @return Fooditem
	 */
	@Override
	public FoodItem createItem(String name, double price, boolean imported,
			int quantity) {
		return new FoodItem(name, price, imported, quantity);
	}

}
