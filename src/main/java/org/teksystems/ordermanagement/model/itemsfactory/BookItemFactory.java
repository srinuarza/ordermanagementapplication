/*
 * 
 */
package org.teksystems.ordermanagement.model.itemsfactory;

import org.teksystems.ordermanagement.model.items.BookItem;

/**
 * The BookItemFactory is a factory class to create a new Bookitem object with
 * specified properties.
 * 
 * @author srinivas
 */
public class BookItemFactory extends ItemFactory {

	/**
	 * {@inheritDoc}
	 * 
	 * @return Bookitem
	 */
	@Override
	public BookItem createItem(String name, double price, boolean imported,
			int quantity) {
		return new BookItem(name, price, imported, quantity);
	}

}
